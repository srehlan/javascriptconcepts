if ("serviceWorker" in navigator) {
  console.log("service worker registration in progress");
  navigator.serviceWorker.register("/js/service-worker.js").then(
    () => {
      console.log("service worker registration complete");
    },
    () => {
      console.log("service worker registration failed");
    }
  );
}
