const cacheName = "v3";

const cacheAssets = [
  "/index.html",
  "/index.about.html",
  "/js/main.js",
  "/css/style.css",
];

self.addEventListener("install", (e) => {
  console.log("service worker installed");
  e.waitUntil(
    caches
      .open(cacheName)
      .then((cache) => {
        console.log(cache);
        console.log("service worker caching files");
        return cache.addAll(cacheAssets);
      })
      .then(() => {
        self.skipWaiting();
      })
  );
});

self.addEventListener("activate", (e) => {
  console.log("service worker : activated");
  e.waitUntil(
    caches.keys().then((cacheName) => {
      return Promise.all(
        cacheName.map((cache) => {
          if (cache != cacheName) {
            console.log("service worker : clearing old cache");
            return caches.delete(cache);
          }
        })
      );
    })
  );
});

self.addEventListener("fetch", (e) => {
  console.log("service worker : fetching ");
  e.respondWith(fetch(e.request).catch(() => caches.match(e.request)));
});
